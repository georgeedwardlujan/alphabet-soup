/**
 * @(#)AlphabetSoup.java
 * @author - George Lujan 
 * @date - 7/2/2023
 * Instructions on Windows: C:\>java AlphabetSoup input_filename.txt
 * Instructions on Linux: $ java AlphabetSoup input_filename.txt
 */
import java.io.*;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class AlphabetSoup {
        
	static int rowTotal;
	static int colTotal;

    public static void main(String[] args)throws IOException {
        
        List<String> fileContentList = new ArrayList<String>();

		// Read from file argument
		Scanner sc = new Scanner(new FileReader(args[0])).useDelimiter("\n");
		String str;

		while (sc.hasNext()) {
			str = sc.next();
			fileContentList.add(str);
		}

		// Parse file and get grid size.
		String[] parsedLine = fileContentList.get(0).split("x");
		rowTotal = Integer.parseInt(parsedLine[0]);
		colTotal = Integer.parseInt(parsedLine[1]);
		
		// Create The Grid
		char[][] gridBox = new char[rowTotal][colTotal];
		for (int x = 0; x < rowTotal; x++) {
			int readLine = x + 1;
            String[] fillGrid = fileContentList.get(readLine).split(" ");
            
            for (int y = 0; y < colTotal; y++) {
                gridBox[x][y] = fillGrid[y].charAt(0);
            }
        }
        
        // Parse out words to search for and loop through..
        List<String> wordList = fileContentList.subList(rowTotal + 1, fileContentList.size());
        
        //Begin series of nested loops to locate words.  currentWord string in in wordList array.
        for (String currentWord : wordList ) {
            for (int r = 0; r < rowTotal; r++) {
                for (int c = 0; c < colTotal; c++) {	
                    // If current letter in grid doesn't match first letter of word to find, continue.
                    if(gridBox[r][c] != currentWord.charAt(0)) {continue; }
                    
                    //8 checks:
                    // 1.Horizontally forwards
                    if (c + currentWord.length() <= colTotal) {
                        for (int i = 0; i < currentWord.length(); i++) {
                            if (gridBox[r][c + i] == currentWord.charAt(i)) {
                                if (i == currentWord.length() - 1)
                                {
                                    //System.out.println("Horizontal Forwards:");
                                    System.out.println( currentWord + " " + r + ":" + c + " " + r + ":" + (c + currentWord.length() - 1) );
                                }
                            }
                            else { break; }
                        }
                    }
                    
                    // 2.Horizontally backwards
                    if (c - currentWord.length()  >= -1) {	//gl
                        for (int i = 0; i < currentWord.length(); i++) {
                            if (gridBox[r][c - i] == currentWord.charAt(i)) {
                                if (i == currentWord.length() - 1)
                                {
                                    //System.out.println("Horizontal Backwards:");
                                    System.out.println(currentWord + " " + r + ":" + c + " " + r + ":" + (c - currentWord.length() + 1));
                                }
                            }
                            else { break; }
                        }
                    }
                    
                    // 3.Vertically downwards
                    if (r + currentWord.length() <= rowTotal) {
                        for (int i = 0; i < currentWord.length(); i++) {
                            if (gridBox[r + i][c] == currentWord.charAt(i)) {
                                if (i == currentWord.length() - 1)
                                {
                                    //System.out.println("Vertical Down:");
                                    System.out.println(currentWord + " " + r + ":" + c + " " + (r + currentWord.length() - 1) + ":" + c);
                                }
                            }
                            else { break; }
                        }
                    }

                    // 4.Vertically upwards
                    if (r - currentWord.length() >= -1) {  //
                        for (int i = 0; i < currentWord.length(); i++) {
                            if (gridBox[r - i][c] == currentWord.charAt(i)) {
                                if (i == currentWord.length() - 1)
                                {
                                    //System.out.println("Vertical Up:");
                                    System.out.println(currentWord + " " + r + ":" + c + " " + (r - currentWord.length() + 1) + ":" + c);
                                }
                            }
                            else { break; }
                        }
                    }

                    // 5.Diagonally (top-left to bottom-right)
                    if (r + currentWord.length() <= rowTotal && c + currentWord.length() <= colTotal) {
                        for (int i = 0; i < currentWord.length(); i++) {
                            if (gridBox[r + i][c + i] == currentWord.charAt(i)) {
                                if (i == currentWord.length() - 1)
                                {
                                    //System.out.println("Diagonal- top-left to bottom-right:");
                                    System.out.println( currentWord + " " + r + ":" + c + " " + (r + currentWord.length() - 1) + ":" + (c + currentWord.length() - 1) );
                                }
                            }
                            else { break; }
                        }
                    }

                    // 6.Diagonally (bottom-right to top-left)
                    if (r >= currentWord.length() - 1 && c >= currentWord.length() - 1) {
                        for (int i = 0; i < currentWord.length(); i++) {
                            if (gridBox[r - i][c - i] == currentWord.charAt(i)) {
                                if (i == currentWord.length() - 1) {
                                	//System.out.println("Diagonal- bottom-right: to top-left:");
                                    System.out.println( currentWord + " " + r + ":" + c + " " + (r - currentWord.length() + 1) + ":" + (c - currentWord.length() + 1) );
                                }
                            }
                            else { break; }
                        }
                    }

                    // 7.Diagonally (bottom-left to top-right)
                    if (r >= currentWord.length() - 1 && c + currentWord.length() <= colTotal) {
                        for (int i = 0; i < currentWord.length(); i++) {
                            if (gridBox[r - i][c + i] == currentWord.charAt(i)) {
                                if (i == currentWord.length() - 1) {
                                	//System.out.println("Diagonal- bottom-left: to top-right:");
                                    System.out.println( currentWord + " " + r + ":" + c + " " + (r - currentWord.length() + 1) + ":" + (c + currentWord.length() - 1) );
                                }
                            }
                            else { break; }
                        }
                    }

                    // 8.Diagonally (top-right to bottom-left)
                    if (r + currentWord.length() <= rowTotal && c - currentWord.length() >= -1) {
                        for (int i = 0; i < currentWord.length(); i++) {
                            if (gridBox[r + i][c - i] == currentWord.charAt(i)) {
                                if (i == currentWord.length() - 1) {
                                    //System.out.println("Diagonal- top-right: to bottom-left:");
                                    System.out.println(currentWord + " " + r + ":" + c + " " + (r + currentWord.length() - 1) + ":" + (c - currentWord.length() + 1));
                                }
                            }
                            else { break; }
                        }
                    } 
                }
            }
        }
    }
}
